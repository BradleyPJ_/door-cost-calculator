(function( $ ) {
    "use strict";

    $(function() {

  /////////////Start Global Variables/////////////

  var $Band = 0;
  var $BandName = "Wood";

  var $TempTotal;
  var $SHeight = 210; // 210cm

  var $SWidth = 100; // 100cm
  var $ElementWidth = $('.Width');
  var $InputWidth;

  var $DefaultCost = 290; // £290
  var $SCost = 290; // £290

  var $TrackCost = 0.33; // £33 divide 100 per Track

  var $Kickboard = false; // No Kickboard by default
  var $KickboardCost = 30; // £30 Per Door
  var $ElementKickboard = $('#Kickboard');

  var $SidePackers = false; // No Sidepackers by default
  var $SidePackerCost = 240; // £240
  var $ElementSidePackers = $('#SidePackers');

  var $DefaultRowLattice = 5; // By Default 5
  var $RowLattice = $DefaultRowLattice;
  var $RowLatticeCost = 8; // £8 For Each Addictional
  var $ElementRowLattice = $('#RowLattice');

  var $DefaultColumnLattice = 1; // By Default 1
  var $ColumnLattice = $DefaultColumnLattice;
  var $ColumnLatticeCost = 12; // £12 For Each Addictional
  var $ElementColumnLattice = $('#ColumnLattice');

  var $NumOfDoors = 0;

  var $Total;

  var $Validation = []; // Errors

  //Image, Title
  var $BandOne = [
    ["Oak.jpeg", "Oak"],
    ["Chestnut.jpeg", "Chestnut"],
    ["Beech.jpeg", "Beech"],
    ["Ash.jpeg", "Ash"],
    ["Bamboo.jpeg", "Bamboo"]
  ];
  var $BandTwo = [
    ["Maple.jpeg", "Maple"],
    ["Elm.jpeg", "Elm"],
    ["Cherry.jpeg", "Cherry"],
    ["Cedar2.jpeg", "Cedar"]
  ];
  var $BandThree = [
    ["Walnut.jpeg", "Walnut"]
  ];


  /////////////End Global Variables/////////////

  /////////////Start Functions/////////////////
  var BandOne = function(){
    $.each($BandOne, function(i){
      $('.SelectWrap').append("<div class='Select' name = '" + $BandOne[i][1] +  "' id = 'Item("+i+")' band = '0' ><img src='../wp/wp-content/themes/Avada/Yet2Learn-Cal/Images/" + $BandOne[i][0] +  "' alt=''><div class='Overlay Hidden'><p>"+ $BandOne[i][1]+"</p></div></div>");
    });
  };

  var BandTwo = function(){
    $.each($BandTwo, function(i){
      $('.SelectWrap').append("<div class='Select' name = '" + $BandTwo[i][1] +  "' id = 'Item("+i+")' band = '10'><img src='../wp/wp-content/themes/Avada/Yet2Learn-Cal/Images/" + $BandTwo[i][0] +  "' alt=''><div class='Overlay Hidden'><p>"+ $BandTwo[i][1]+"</p></div></div>");
    });
  };

  var BandThree = function(){
    $.each($BandThree, function(i){
      $('.SelectWrap').append("<div class='Select' name = '" + $BandThree[i][1] +  "'id = 'Item("+i+")' band = '20' ><img src='../wp/wp-content/themes/Avada/Yet2Learn-Cal/Images/" + $BandThree[i][0] +  "' alt=''><div class='Overlay Hidden'><p>"+ $BandThree[i][1]+"</p></div></div>");
    });
  };

  BandOne();
  BandTwo();
  BandThree();

  $('#TabOne').click(function() {
    $('.SelectWrap').empty();
      BandOne();
  });
  $('#TabTwo').click(function() {
    $('.SelectWrap').empty();
      BandTwo();
  });
  $('#TabThree').click(function() {
    $('.SelectWrap').empty();
      BandThree();
  });

  $('#TabAll').click(function() {
    $('.SelectWrap').empty();
      BandOne();
      BandTwo();
      BandThree();
  });

  $('.SelectWrap').on('click', '.Select', function() {

    //Removes "Selected" Class then adds Class to the clicked div
    $('.Select').not(this).removeClass('Selected');
    console.log(this);
    $(this).addClass("Selected");

    //When class "Select" is clicked take the attr (attribute) from the Element and
    //give them to a variable. this used in later calculations.
    $Band = $(this).attr("band");
    $BandName = $(this).attr("name");

    Update();

  });

  $('.Submit').click(function() {
    Update();
  });

  //Anything element with the attr role and the result Update when anything happens to it, it runs Update
  $("[role='Update']").on('change',function() {
    Update();
  });

  var Update = function() {

    $SHeight = $('#DoorHeight').val();

    if ($SHeight <= 210) {
      $SCost = $DefaultCost;
    } else if ($SHeight > 210 && $SHeight <= 240) {
      $SCost = ($DefaultCost + ($DefaultCost / 10));
    }

    //Gets Values from html form
    $InputWidth = $ElementWidth.val();

    //Converts Stirng Numbers in to Ints
    //$InputHeight = parseFloat($InputHeight);
    $InputWidth = parseFloat($InputWidth);

    //Rounds the value to the next whole number because can't purchase a half
    //var $NumOfHeight = Math.ceil($InputHeight / $SHeight);
    var $NumOfWidth = Math.ceil($InputWidth / $SWidth);

    $NumOfDoors = $NumOfWidth;

    //If checkbox checked = true
    $Kickboard = $ElementKickboard.is(':checked');
    $SidePackers = $ElementSidePackers.is(':checked');

    var $Tracks = ($NumOfDoors * ($InputWidth) * $TrackCost);
    console.log($Tracks);
    $Tracks = Math.round($Tracks*100)/100;


        $TempTotal = (($NumOfDoors * $SCost) + $Tracks + (Kickboard()) + (RowLattice()) + (ColumnLattice()) + SidePackers());
        $Total = $TempTotal + Band();

        //$Total = Math.round($Total*100)/100;
    	  $Total = Math.round($Total);

        //Quote output all the costs in to a Quote
        if ($.isNumeric($InputWidth)) {


            $('.Output').empty();
            $('.Output').html(

              '<div class="wrap">' +

              '<table style="width:100%">' +
              '<tr> <td>  </td> <td> Dimensions </td> </tr>' +
              '<tr> <td> Width </td> <td> ' + $InputWidth + 'cm </td> </tr>' +
              '</table></br></br>' +

              '<table style="width:100%">' +
              '<tr> <td>  </td> <td> Dimensions </td> </tr>' +
              '<tr> <td> Height </td> <td> Less Than ' + $SHeight + 'cm </td> </tr>' +
              '</table></br></br>' +


              '<table style="width:100%">' +
              '<tr> <td>  </td> <td> Type </td> <td> Percentage </td> <td> Cost </td> </tr>' +
              '<tr> <td> Band </td> <td> ' + $BandName + ' </td> <td> ' + $Band + '% </td> <td> £' +  Band() + ' </td> </tr>' +
              '</table></br></br>' +

              '<table style="width:100%">' +
              '<tr> <td>  </td> <td> Quantity </td> <td> Price </td> <td> Cost </td> </tr>' +
              '<tr> <td> Doors </td> <td> ' + $NumOfDoors + ' </td> <td> £' + $SCost + ' </td> <td> £' + ($NumOfDoors * $SCost) + ' </td> </tr>' +
              '<tr> <td> Tracks </td> <td> ' + $NumOfDoors + ' </td> <td> £' + $TrackCost * 100 + ' </td> <td> £' + Math.round($Tracks) + ' </td> </tr>' +
              '<tr> <td> RowLattice </td> <td> ' + $RowLattice + ' </td> <td> £' + $RowLatticeCost + ' </td> <td> £' + RowLattice() + ' </td> </tr>' +
              '<tr> <td> ColumnLattice </td> <td> ' + $ColumnLattice + ' </td> <td> £' + $ColumnLatticeCost + ' </td> <td> £' + ColumnLattice() + ' </td> </tr>' +
              '<tr> <td> Kickboards </td> <td> ' + Kickboard("num") + ' </td> <td> £' + $KickboardCost + ' </td> <td> £' + Kickboard() + ' </td> </tr>' +
              '<tr> <td> SidePackers </td> <td> ' + SidePackers("num") + ' </td> <td> £' + $SidePackerCost + ' </td> <td> £' + SidePackers() + ' </td> </tr>' +
              '</table>' +
              '<p> Total: £' + $Total + '</p>' +
              '<p>Please Enter you Email</p>' +
              '<input   type="email"  placeholder="Enter Email">'+
              '<textarea name="Message" rows="8" cols="40"></textarea>'+
              '</div>'
            );
        }
    ErrorManager();


  };

  var ErrorManager = function() {

    //Checks then pushes to Error Array, Sets error color for the affected elements
    if (!$('.Select').hasClass('Selected')) {
      $Validation.push("* Please Select Wood Band");
      $('.Select').css("border", "1px solid red");
    } else {
      $('.Select').css("border", "1px solid rgb(169, 169, 169)");
    }

    if (!$.isNumeric($InputWidth)) {
      $Validation.push("* Please Enter Width as a Number (cm)");
      $ElementWidth.css("border", "1px solid red");
    } else {
      $ElementWidth.css("border", "1px solid rgb(169, 169, 169)");
    }

    // Remove everything inside #Error including Elements
    $('.Error').empty();

    //For each error print error
    $.each($Validation, function(i) {
      $('.Error').append("<p class = 'error' id = 'error(" + i + ")'>" + $Validation[i] + "</p>");
    });

    if ($Validation.length > 0) {
      $('.Error').addClass('Show');
    } else {
      $('.Error').removeClass('Show');
    }

    //Clears Error Log/Array
    $Validation = [];
  };

  //Finds the percentage from the band and total
  var Band = function() {

    //For when we dont want the total to change
    if ($('.Select').hasClass('Selected')) {


      $Band = parseFloat($Band);
      var j = (($TempTotal / 100) * $Band);

	    j = Math.round(j);


      return j;
    } else {
      return 0;
    }
  };

  // Returns cost of a kickboard if the customer selects a kickboard
  var Kickboard = function(num) {

    switch (num) {
      case "num":
        if ($Kickboard) {
          return $NumOfDoors;
        } else {
          return 0;
        }
        break;

      default:
        if ($Kickboard) {
          return $KickboardCost * $NumOfDoors;
        } else {
          return 0;
        }
    }
  };

  var SidePackers = function(num) {

    switch (num) {
      case "num":
        if ($NumOfDoors >= 1 && $SidePackers) {
          return 2;
        } else {
          return 0;
        }
        break;

      default:
        if ($NumOfDoors >= 1 && $SidePackers) {
          return $SidePackerCost;
        } else {
          return 0;
        }
    }

  };

  var RowLattice = function() {
    if ($RowLattice >= $DefaultRowLattice) {

      //Takes value from html dropdown
      $RowLattice = $ElementRowLattice.val();

      var i = $RowLattice - $DefaultRowLattice; //Finds additional lattice number
      var j = (i * $RowLatticeCost) * $NumOfDoors;

      return j;
    } else {
      return 0;
    }
  };

  var ColumnLattice = function() {
    if ($ColumnLattice >= $DefaultColumnLattice) {

      //Takes value from html dropdown
      $ColumnLattice = $ElementColumnLattice.val();

      var i = $ColumnLattice - $DefaultColumnLattice; //Finds additional lattice number
      var j = (i * $ColumnLatticeCost) * $NumOfDoors;

      return j;
    } else {
      return 0;
    }
  };



  /////////////End Functions/////////////

    });

}(jQuery));
