<?php
// Template Name: Yet2Learn
get_header(); ?>
	<?php
	$content_css = 'width:100%';
	$sidebar_css = 'display:none';
	$content_class = '';
	$sidebar_exists = false;
	$sidebar_left = '';
	$double_sidebars = false;
	?>

<div role="Update" class="CalContainer">


	<div class="Error Hidden"></div>

	<div class="CalContainer">
		<div class="wrap">

		<p style = "text-align:center;">Width cm</p>
		<input   type="number" class = "Width" placeholder="Width cm">

		<p style = "text-align:center;">Door Height</p>
		<select id = "DoorHeight" >
			<option value="210">Up to 210cm Default</option>
			<option value="240">Up to 240cm Addictional 10%</option>
		</select>

		<div class="Options">
			<input  style = "float:left; margin:20px;" id = "Kickboard" type="checkbox" value="true"><p style = "float:left;"> Add Kickboard + £30 Per Door</p>
			<br>
			<br>
			<br>
			<input  style = "float:left; margin:20px;" id = "SidePackers" type="checkbox" value="true"><p style = "float:left;"> Add Sidepackers + £240</p>
		</div>
		<div class="Lattice">
				<p style = "clear:both;">Row</p>
				<select id = "RowLattice" >
					<option value="5">Default 5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
		</div>

		<div class="Lattice" style = "float:right;">
			<p>Column</p>
			<select id = "ColumnLattice" >
				<option value="1">Default 1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
			</select>
		</div>
		<br>
		<br>


		<p style = "Clear:both;"><a href="tel:01243-376610">Call 0281764237 to Order!</a></p>
		<button type="submit" value = "send" style = "Margin:auto 20px;" class = "Submit" role = "Update" >Get Quote</button>
		</div>


		<div class="Output " style = "float:right;">
			<div class="wrap">

			<table style="width:100%">
			<tr> <td>  </td> <td> Dimensions </td> </tr>
			<tr> <td> Width </td> <td> 0cm </td> </tr>
			</table></br></br>

			<table style="width:100%">
			<tr> <td>  </td> <td> Dimensions </td> </tr>
			<tr> <td> Height </td> <td> Less Than 0cm </td> </tr>
			</table></br></br>


			<table style="width:100%">
			<tr> <td>  </td> <td> Type </td> <td> Percentage </td> <td> Cost </td> </tr>
			<tr> <td> Band </td> <td> Wood </td> <td> 0% </td> <td> £0 </td> </tr>
			</table></br></br>

			<table style="width:100%">
			<tr> <td>  </td> <td> Quantity </td> <td> Price </td> <td> Cost </td> </tr>
			<tr> <td> Doors </td> <td> 0 </td> <td> £0 </td> <td> £0 </td> </tr>
			<tr> <td> Tracks </td> <td> 0 </td> <td> £0 </td> <td> £0 </td> </tr>
			<tr> <td> RowLattice </td> <td> 0 </td> <td> £0 </td> <td> £0 </td> </tr>
			<tr> <td> ColumnLattice </td> <td> 0 </td> <td> £0 </td> <td> £0 </td> </tr>
			<tr> <td> Kickboards </td> <td> 0 </td> <td> £0 </td> <td> £0 </td> </tr>
			<tr> <td> SidePackers </td> <td> 0 </td> <td> £0 </td> <td> £0 </td> </tr>
			</table>
			<p> Total: £0</p>
			<p>
				Please Enter you Email
			</p>
			<input   type="email"  placeholder="Enter Email">
			<textarea name="Message" rows="8" cols="40"></textarea>
			</div>
		</div>
	</div>


<div class="BandMenu">

	<div class="SelectMenu">
		<li><a id = "TabAll"> All Bands</a></li>
		<li><a id = "TabOne"> Band 1 </a></li>
		<li><a id = "TabTwo"> Band 2 </a></li>
		<li><a id = "TabThree"> Band 3 </a></li>
	</div>

	<div class="SelectWrap wrap ">
	</div>
</div>
</div>


 <?php get_footer(); ?>
